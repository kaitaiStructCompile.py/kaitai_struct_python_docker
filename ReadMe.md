Scripts for building an image containing CPython, GraalVM and Kaitai Struct
===========================================================

Scripts in this repo are [Unlicensed ![](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/).

Third-party components have own licenses.

**DISCLAIMER: BADGES BELOW DO NOT REFLECT THE STATE OF THE DEPENDENCIES IN THE CONTAINER**

This builds a Docker image contains the following components:

* [`registry.gitlab.com/kolanich/python_graal_docker:latest`](https://gitlab.com/KOLANICH/python_graal_docker)  - a Docker image, each component of which has own license;
* [BinTray public key](https://bintray.com/user/downloadSubjectPublicKey?username=bintray) (fingerprint: `8756C4F765C9AC3CB6B85D62379CE192D401AB61`) [from `hkps://keyserver.ubuntu.com`](https://keyserver.ubuntu.com/pks/lookup?search=0x8756C4F765C9AC3CB6B85D62379CE192D401AB61&op=get)
* [`kaitai-struct-compiler`](https://github.com/kaitai-io/kaitai_struct_compiler) [![](https://repology.org/badge/version-for-repo/kaitai-struct-compiler/swig.svg)](https://repology.org/metapackage/kaitai-struct-compiler/versions) [![latest packaged version(s)](https://repology.org/badge/latest-versions/kaitai-struct-compiler.svg)](https://repology.org/metapackage/kaitai-struct-compiler/versions) deb package from [BinTray](https://dl.bintray.com/kaitai-io/debian_unstable) [![GNU General Public License v3](https://www.gnu.org/graphics/gplv3-88x31.png)](./licenses/GPL-3.0.txt);
* `kaitaistruct` python package implementing the runtime. [![PyPi Status](https://img.shields.io/pypi/v/kaitaistruct.svg)](https://pypi.python.org/pypi/kaitaistruct) [![Licence](https://img.shields.io/github/license/kaitai-io/kaitai_struct_python_runtime.svg)](https://github.com/kaitai-io/kaitai_struct_python_runtime/blob/master/README.rst) [![Libraries.io Status](https://img.shields.io/librariesio/github/kaitai-io/kaitai_struct_python_runtime.svg)](https://libraries.io/github/kaitai-io/kaitai_struct_python_runtime) [from git](https://github.com/kaitai-io/kaitai_struct_python_runtime)
* `kaitaiStructCompile.py` [![Unlicensed](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/) [from git](https://github.com/kaitaiStructCompile.py/kaitaiStructCompile.py), including
    * `kaitaiStructCompile.backend.CLI` [![Unlicensed](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/) [from git](https://github.com/kaitaiStructCompile.py/kaitaiStructCompile.backend.CLI)
* `RichConsole` [![Unlicensed](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/) [from git](https://github.com/KOLANICH/RichConsole.py) for colorful output during compilation
* dependencies of everytihing above.
