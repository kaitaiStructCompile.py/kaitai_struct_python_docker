# syntax=docker/dockerfile:experimental
FROM registry.gitlab.com/kolanich-subgroups/docker-images/python_graal:latest
LABEL maintainer="KOLANICH"
WORKDIR /tmp
ADD ./*.sh ./
RUN  \
  set -ex; \
  ls -lh ; \
  apt-get update ; \
  . ./setupKS.sh ; \
  pip3 install --upgrade --pre git+https://github.com/gitpython-developers/smmap.git git+https://github.com/gitpython-developers/gitdb.git git+https://github.com/gitpython-developers/GitPython.git git+https://gitlab.com/KOLANICH/RichConsole.git git+https://gitlab.com/kaitaiStructCompile.py/kaitaiStructCompile.py.git ; \
  apt-get autoremove --purge -y ; \
  apt-get clean && rm -rf /var/lib/apt/lists/* ; \
  rm -r ~/.cache/pip ; \
  rm -r /tmp/*
ENV KAITAI_STRUCT_ROOT $KAITAI_STRUCT_ROOT
